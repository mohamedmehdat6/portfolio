import { AContent } from "@/components/content";
import { Header } from "@/components/header";
import { SideBar } from "@/components/sidebar";

export default function Home() {
  return (
    <main className="">
      <Header  />
      <SideBar />
      <AContent />
    </main>
  )
}
