import Image from "next/image";
import myavatar from "./avatar.png";
import { WindowedDiv } from "./windoweddiv";
export const Introduction = () => {
return (<div className=" flex xs:flex-col md:flex-row justify-center  items-center">
        <WindowedDiv className=" xs:w-11/12 xs:mx-auto md:mx-5 md:w-auto" title="Me" id="intorduction">
        <Image 
        src={myavatar}
        alt="My Avatar"
        className="rounded-full mx-auto w-72 h-72"
        />
        </WindowedDiv>
        <WindowedDiv title="About Me" className="xs:mt-5 xs:w-11/12 md:w-auto md:mx-5">
        <div className="font-pixelated">
        <p className="font-pixelated text-8xl animate-bounce tracking-wide">About me </p>
        <div  className="border-b-2 border-black mb-5"/>
        <p className="w-52 text-xl">Hello, My name is <span className=" tracking-widest text-2xl text-gray-700">Mohamed Medhat</span> , I am a frontend developer specializing in creating beuatiful UI and experiences for the users</p>
        <p className="w-52 text-xl">I like creative challenges and making software that affects people life</p>
        </div>
        </WindowedDiv>

    </div>)
}