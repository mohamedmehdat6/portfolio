import { WindowedDiv } from "./windoweddiv"

export const WorkExperience = () => {
    return (<WindowedDiv id="workexperience" title="My Work Experience" className="ml-auto mr-auto w-11/12 mt-10  ">
        <div className="w-full text-center text-6xl font-pixelated">My Work Experience</div>
        <div className="w-full border-b-2 border-black mt-3"></div>
        <div className="flex md:flex-row xs:flex-col">
        <div className="transition ease-in-out   border-2 md:w-1/2 xs:w-full h-96 flex flex-col justify-center md:m-5 xs:mt-2 border-black hover:bg-black hover:text-white text-center hover:scale-105">
            <p className="text-5xl font-pixelated ">Digital Zone</p>
            <p className="text-3xl font-pixelated ">Frontend Developer - React</p>
            <p className="text-xl font-pixelated ">2021-2022</p>
        </div>
        <div className="transition ease-in-out   border-2 md:w-1/2 xs:w-full h-96 flex flex-col justify-center md:m-5 xs:mt-5 border-black hover:bg-black hover:text-white text-center hover:scale-105">
            <p className="text-5xl font-pixelated ">Informatique</p>
            <p className="text-3xl font-pixelated ">Frontend Developer - React</p>
            <p className="text-xl font-pixelated ">2022-present</p>
        </div>
        </div>
    
    </WindowedDiv>)
}