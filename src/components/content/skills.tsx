import { WindowedDiv } from "./windoweddiv"
export const Skills = () => {
    return (
        <WindowedDiv id="skills" title="My Skills" className="ml-auto mr-auto w-11/12  xs:mt-32 md:mt-32 ">
            <div className="w-full text-center text-6xl font-pixelated">My Skills</div>
            <div className="w-full border-b-2 border-black mt-3"></div>

        <div className="flex md:flex-row xs:flex-col">
        <div className="transition ease-in-out   border-2 md:w-1/4 xs:w-full h-44 flex flex-col justify-center md:m-5 xs:mt-5 border-black hover:bg-black hover:text-white text-center hover:scale-105">
            <p className="text-5xl font-pixelated ">React</p>
        </div>

        <div className="transition ease-in-out   border-2 md:w-1/4 xs:w-full h-44 flex flex-col justify-center md:m-5 xs:mt-5  border-black hover:bg-black hover:text-white text-center hover:scale-105">
            <p className="text-5xl font-pixelated ">Typescript</p>
        </div>
        <div className="transition ease-in-out   border-2 md:w-1/4 xs:w-full h-44 flex flex-col justify-center md:m-5  xs:mt-5 border-black hover:bg-black hover:text-white text-center hover:scale-105">
            <p className="text-5xl font-pixelated ">NextJS</p>
        </div>
        <div className="transition ease-in-out   border-2 md:w-1/4 xs:w-full h-44 flex flex-col justify-center md:m-5  xs:mt-5 border-black hover:bg-black hover:text-white text-center hover:scale-105">
            <p className="text-5xl font-pixelated ">C++</p>
        </div>
        </div>
        </WindowedDiv>
    )
}