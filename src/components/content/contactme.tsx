"use client";
import { WindowedDiv } from "./windoweddiv"

export const ContactMe = () => {
    return (<WindowedDiv id="contact" title="Contact Me" className="md:mt-32 ml-auto mr-auto w-11/12 xs:mt-32 ">

        <div className="flex md:flex-row xs:flex-col">
            <div className="transition ease-in-out   border-2 md:w-1/4 xs:w-full h-44 flex flex-col justify-center md:m-5 xs:mt-5 border-black hover:bg-black hover:text-white text-center hover:scale-105 cursor-pointer ">
                <p className="text-5xl font-pixelated ">Phone / Whatsapp</p>
                <p className="text-3xl font-pixelated ">01069558430</p>
            </div>

            <div onClick={() => {
                window.open("https://www.linkedin.com/in/mohamed-mehdat-b473a7279/","_blank")?.focus();
            }} className="transition ease-in-out   border-2 md:w-1/4 xs:w-full h-44 flex flex-col justify-center md:m-5 xs:mt-5 border-black hover:bg-black hover:text-white text-center hover:scale-105 cursor-pointer">
                <p className="text-5xl font-pixelated ">LinkedIn</p>
                <p className="text-3xl font-pixelated ">Click Here</p>
            </div>
        </div>
    </WindowedDiv>)
}