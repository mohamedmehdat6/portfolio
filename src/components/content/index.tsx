import { ContactMe } from "./contactme"
import { Introduction } from "./introduction"
import { Skills } from "./skills"
import { WorkExperience } from "./workexperience"

export const AContent = () => {
    return (
        <>
            <div id="upperbar" className="fixed right-0 top-24 md:left-52 xs:left-0 h-7 border-b-2 border-y-black flex justify-end items-center" >
                <div className="w-4 border-2 border-black h-4 rounded-full m-1 text-center hover:bg-red-600"></div>
                <div className="w-4 border-2 border-black h-4 rounded-full m-1 text-center hover:bg-red-600"></div>
                <div className="w-4 border-2 border-black h-4 rounded-full m-1 text-center hover:bg-red-600"></div>
            </div>
            <div id="content" className="bg-[#F0EAE8] overflow-scroll overflow-x-hidden text-black fixed bottom-0 top-32 md:left-52 right-0 xs:left-0   h-100"
       
            > 
                <Introduction />
                <WorkExperience />
                <Skills />
                <ContactMe />
            </div>
        </>
    )

}