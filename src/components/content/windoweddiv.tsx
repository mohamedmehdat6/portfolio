export const WindowedDiv: React.FC<React.PropsWithChildren<{className? : string,title? : string,id? : string}>> = (props) => {
    const { children,className ,title,id} = props;
    return (
        
        <div className={className} id={id}>
        <div id="upperbar" className=" h-7 border-2 border-black flex justify-end items-center" >
           <div className="mr-auto ml-2 font-pixelated">{title}</div>
            <div className="w-4 border-2 border-black h-4 rounded-full m-1 text-center hover:bg-red-600"></div>
            <div className="w-4 border-2 border-black h-4 rounded-full m-1 text-center hover:bg-red-600"></div>
            <div className="w-4 border-2 border-black h-4 rounded-full m-1 text-center hover:bg-red-600"></div>
        </div>
        <div className="border-2 border-black border-t-0 p-10">
        {children}
        </div>
        </div>

    )

}