"use client"
import { Link, Button, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

const SideBarItems = [
    { label: "Introduction", id: "intorduction" },
    { label: "Work Experience", id: "workexperience" },
    { label: "Skills", id: "skills" },
    { label: "Contect Me", id: "contact" },
]
const GoToSection = (id: string) => {
    const content = document.getElementById("content");
    content?.scrollTo({ top: document.getElementById(id)?.offsetTop, behavior: "smooth" });
}
export const SideBar = () => {

    return (
        <div className="w-52  xs:hidden md:block  h-screen fixed top-24 border-r-2 border-r-black">
            <nav className="text-black  border-t-black">
                <ul>
                    {SideBarItems.map((x,i) => (
                        <li key={i} onClick={() => {GoToSection(x.id)}} className=" transition ease-in-out  font-pixelated hover:bg-black hover:text-white tracking-wider  border-b-2 border-b-black p-3">
                            {x.label}
                        </li>))}
                </ul>
            </nav>
        </div>)
}