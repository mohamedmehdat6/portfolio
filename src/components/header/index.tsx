export const Header = () => {
    return (<div className="fixed top-0 left-0 right-0 h-24  flex justify-center border-b-2 border-b-black">
        <div className="mt-2">
        <h2 className="text-slate-950 font-pixelated text-4xl text-center tracking-wider"> Mohamed Medhat</h2>
        <h2 className="text-gray-700 font-pixelated text-xl text-center"> Frontend Developer</h2>
        </div>
    </div>)
}